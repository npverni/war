require 'spec_helper'

describe Game do

  describe '#initialize' do
    let!(:game) { described_class.new }

    it 'instantiates players' do
      expect(game.p1.is_a?(Player)).to be_truthy
      expect(game.p2.is_a?(Player)).to be_truthy
    end
  end

  describe '#deal' do
    let!(:game) { described_class.new.tap(&:deal) }
    it 'deals 26 cards to each player' do
      expect(game.p1.hand.count).to eql(26)
      expect(game.p2.hand.count).to eql(26)
    end
  end

end
