require 'spec_helper'

describe Deck do
  it 'has 52 cards' do
    expect(Deck.new.cards.count).to eql(52)
  end

  describe '#deal' do
    it 'splits the cards into equal groups' do
      hand_1, hand_2 = Deck.new.deal(2)
      expect(hand_1.count).to eql(26)
      expect(hand_2.count).to eql(26)
    end
  end
end
