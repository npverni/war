require 'spec_helper'

describe Player do

  describe '.play_card' do
    it 'moves a card from the hand to the pot' do
      p = Player.new
      p.hand = [1, 2, 3]
      p.pot  = [7, 8]
      p.play_card
      expect(p.hand).to eql([2, 3])
      expect(p.pot).to  eql([1, 7, 8])
    end
  end

  describe '.collect_from' do
    it "collects another player's cards and moves it's own pot to it's hand" do
      p1, p2 = Player.new, Player.new
      p1.hand = [1, 2, 3]
      p1.pot  = [4, 5, 6]
      p2.hand = [7, 8, 9]
      p2.pot =  [10, 11, 12]

      p1.collect_from(p2)

      expect(p1.hand.count).to eql(9)
      expect(p2.hand.count).to eql(3)
      expect(p2.pot.count).to  eql(0)
      expect(p2.pot.count).to  eql(0)
      expect(p1.hand[0,3]).to  eql([1, 2, 3])
    end
  end
end
