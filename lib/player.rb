class Player

  attr_accessor :hand, :pot

  def initialize
    self.hand = []
    self.pot  = []
  end

  def play_card
    self.pot.unshift hand.shift
  end

  def collect_from(other_player)
    cards = [self.pot + other_player.pot].flatten.shuffle
    self.hand.push(*cards)
    self.pot = []
    other_player.pot = []
  end

  def has_cards?
    self.hand.any?
  end
end
