class Game
  attr_accessor :p1, :p2, :num_hands, :num_wars, :debug

  def self.run(debug=false)
    game = self.new(debug)
    game.deal
    game.run
    game
  end

  def initialize(debug=false)
    self.debug     = debug
    self.p1        = Player.new
    self.p2        = Player.new
    self.num_hands = 0
    self.num_wars  = 0
  end

  def run
    while p1.has_cards? && p2.has_cards?
      run_hand
    end
    p "Number of hands: #{num_hands}"
    p "Number of wars:  #{num_wars}"
  end


  def deal
    deck = Deck.shuffled
    p1.hand, p2.hand = deck.deal(2)
  end

  def flip
    p1.play_card
    p2.play_card
  end

  def compare
    p1_card, p2_card = p1.pot.first, p2.pot.first

    if p1_card.nil? || p2_card.nil?
      return
    elsif p1_card == p2_card
      self.num_wars = num_wars + 1
      p "WAR" if debug
      flip #burn a card
      run_hand
    elsif p1_card > p2_card
      p1.collect_from(p2)
    else
      p2.collect_from(p1)
    end
  end

  def run_hand
    self.num_hands = num_hands + 1
    if debug
      p "Hand #{num_hands}"
      p p1.hand
      p p2.hand
    end
    flip
    p "pot: #{pot}" if debug
    compare
  end

  def pot
    [p1.pot + p2.pot].flatten
  end

end
