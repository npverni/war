class Deck

  attr_reader :cards

  def self.shuffled
    self.new.tap(&:shuffle)
  end

  def initialize
    @cards = []
    4.times{ @cards += (1..13).to_a }
  end

  def shuffle
    @cards.shuffle!
  end

  def deal(num_players)
    @cards.each_slice(@cards.count/num_players).to_a
  end

end
